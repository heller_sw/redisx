package com.redissdk.redis;

/**
  * 项目名称:[redisx]
  * 包:[com.redissdk.redis]    
  * 文件名称:[JRedisCacheException]  
  * 描述:[JRedisCacheException - 异常类]
  * 创建人:[陆文斌]
  * 创建时间:[2016年12月5日 下午5:50:12]   
  * 修改人:[陆文斌]   
  * 修改时间:[2016年12月5日 下午5:50:12]   
  * 修改备注:[说明本次修改内容]  
  * 版权所有:luwenbin006@163.com
  * 版本:[v1.0]
 */
class JRedisCacheException extends Exception {

	public JRedisCacheException(Exception ex) {
		// TODO Auto-generated constructor stub
		super(ex);
	}

	public JRedisCacheException() {
		// TODO Auto-generated constructor stub
		super();
	}

	public JRedisCacheException(String string) {
		// TODO Auto-generated constructor stub
		super(string);
	}
}
